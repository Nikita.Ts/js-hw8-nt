// 1. Опишіть своїми словами що таке Document Object Model (DOM)
Це незалежний від платформи та мови програмний інтервейс, що дозволяє програмам і скриптам 
отримати доступ до вмісту HTML документів, а також змінювати вміст, структуру та оформлення таких документів. 
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerHTML повертає всі дочірні елемент, що вкладені всередині тега (тег HTML + текстовий вміст). 
Проаналізує отриманий контент  та інтерпритує теги. 
innerText повертає текстовий вміст дочірніх елементів, які вкладені всередині мітки. 
Буде читати все як текст. 
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
getElementById (); getElementsByTagName (); getElementsByClassName (); querySelector (); querySelectorAll ().
Залежить від задачі, але найпростішим і найкращим є перший варіант getElementById. 