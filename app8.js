// 1
document.querySelectorAll('p').forEach(e => e.style.backgroundColor = '#ff0000');
// 2
let optionList = document.getElementById('optionList');
console.log('option list', optionsList);
console.log('option list parent node', optionsList.parentNode);
if (optionsList.hasChildNodes()) {
    let children = optionsList.childNodes;
    for (let i = 0; i < children.length; ++i) {
        console.log(children[i].nodeType);
    }
}
// 3
let testParagraph = document.getElementById('testParagraph');
testParagraph.textContent = 'This is a paragraph'

// 4, 5 
let mainHeader = document.getElementsByClassName('main-header')[0];

if (mainHeader.hasChildNodes()) {
    let children = mainHeader.childNodes;
    for (let i = 0; i < children.length; ++i) {
        console.log('main-header children' +i, children[i]);
        if (children[i].classList) {
            children[i].classList.add('nav-item');
        }
    }
}

// 6 
let sectionTitle = document.querySelectorAll('.section-title'); 
sectionTitle.forEach(e => e.classList.remove('section-title'));  